#!/bin/bash
# File Name   : ../function/logging.sh
# Author      : moshan
# Mail        : mo_shan@yeah.net
# Created Time: 2019-04-15 16:42:03
# Function    : 
#########################################################################
work_dir=/data/git/analysis_binlog
function f_logging()
{
	#moshan:日志函数, 接收四个参数, 第一个表示日志级别, 第二个表示日志内容, 第三个表示是否需要回车(0|1|2,0表示不需要回车,1表示需要一个回车),2表示需要两个回车)
	#moshan:usage:f_logging "INFO|WARNERROR|COMMAND" "This is a log" "-n|NULL" "0|NULL"
	log_mode="${1}"
	log_info="${2}"
	log_enter="${3}"
	enter_opt=""        #moshan:表示回车的动作
	if [ "${log_mode}x" == "WARNx" ]
	then
		#moshan:WARN级别是黄色显示
		echo -e "\033[33m"
	elif [ "${log_mode}x" == "ERRORx" ]
	then
		#moshan:ERROR级别是红色显示
		echo -e "\033[31m"
	elif [ "${log_mode}x" == "COMMANDx" ]
	then
		#moshan:COMMAND级别是蓝色显示
		echo -en "\033[34m"
	else
		#moshan:INFO级别是绿色显示
		echo -en "\033[32m"
	fi
	if [ "${log_enter}x" == "0x" ]
	then
		log_enter="-n"
	elif [ "${log_enter}x" == "2x" ]
	then
		log_enter="-e"
		enter_opt="\n"
	else
		#moshan:相当于值是1,即这是默认值
		log_enter="-e"
	fi
	echo ${log_enter} "[$(date "+%F %H:%M:%S")] [${log_mode}] [${localhost_ip}] ${log_info}${enter_opt}"
	echo -en "\033[0m"
	echo -en "\033[32m"
}
